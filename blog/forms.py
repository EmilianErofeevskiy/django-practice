from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User

class ContactForm(forms.Form):
    name = forms.CharField(label="Ф.И.О.", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    email = forms.EmailField(label="Email", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    phone = forms.CharField(label="Телефон", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    text = forms.CharField(label="Сообщение", widget=forms.Textarea(attrs={'class' : 'form-control footer-input margin-b-30'}))

class RegisterForm(UserCreationForm):
    # declare the fields you will show
    username = forms.CharField(label="Никнейм", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    # first password field
    password1 = forms.CharField(label="Пароль", required=True, widget=forms.PasswordInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    # confirm password field
    password2 = forms.CharField(label="Повторите пароль", required=True, widget=forms.PasswordInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    email = forms.EmailField(label = "Email", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    first_name = forms.CharField(label = "Имя", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
    last_name = forms.CharField(label = "Фамилия", required=True, widget=forms.TextInput(attrs={'class' : 'form-control footer-input margin-b-20'}))
 
    # this sets the order of the fields
    class Meta:
        model = User
        widgets = {'password1': forms.PasswordInput(), 'password2': forms.PasswordInput(),}
        fields = ("first_name", "last_name", "email", "username", "password1", "password2", )
 
    # this redefines the save function to include the fields you added
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
 
        if commit:
            user.save()
            return user