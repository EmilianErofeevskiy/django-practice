from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse, HttpResponseRedirect
from blog.models import Comment, Post, Category, Contact
from django.core.mail import send_mail, BadHeaderError
from .forms import ContactForm, RegisterForm
from django.views.generic import ListView
from django.db.models import Q
from django.utils import timezone
from django.contrib.auth.models import User


def index(request):
    posts = Post.objects.all()[:10]
    latest_three = Post.objects.order_by('-date')[:3]
    context = {'posts': posts, 'latest_three': latest_three}
    return render(request, 'blog/welcome.html', context)

def posts(request):
    posts = Post.objects.order_by('-date')[:10]
    categories = Category.objects.all()[:10]
    context = {'posts':posts, 'categories':categories}
    return render(request, 'blog/news.html', context)

def detail(request, slug):
    try:
        post = Post.objects.get(slug = slug)
        categories = Category.objects.all()[:10]
        context = {'post': post, 'categories':categories}
    except Post.DoesNotExist:
        raise Http404("Такого поста не существует")
    return render(request, 'blog/detail.html', context)

def cat_detail(request, slug):
    try:
        cat = Category.objects.get(slug = slug)
        categories = Category.objects.all()[:10]
        context = {'cat':cat, 'categories':categories}
    except Category.DoesNotExist:
        raise Http404("Такой категории не существует")
    return render(request, 'blog/cat_detail.html', context)

def contact_cont(request):
    form = ContactForm(request.POST)
    if form.is_valid():
                name = form.cleaned_data['name']
                email = form.cleaned_data['email']
                phone = form.cleaned_data['phone']
                text = form.cleaned_data['text']
                # try:
                #     send_mail(name, email, phone, text, ['emilerofeevskij@gmail.com'])
                # except BadHeaderError:
                #     return HttpResponse('Invalid header found.')
                c = Contact(name = name, email = email, phone = phone, text = text, date = timezone.now())
                c.save()
    return redirect('index') #don't forget to return args

# Search query from Post model, inherit from db.models Q at the top, takes query from form into the layout
def search_result(request):
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains=query) | Q(summary__icontains=query)
    )
    return render(request, 'blog/search.html', {'search_obj':search_obj, 'query':query})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            #log the user in
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'blog/register.html', {'form':form})# inside urls/ forms / and template
