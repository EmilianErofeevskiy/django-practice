from django.db import models
from django.utils.text import slugify
from django.utils import timezone
from django.urls import reverse

class Category(models.Model):
    title = models.CharField("Имя категории", max_length=255)
    image = models.ImageField("Картинка категории", null=True, blank=True, upload_to='categories/')
    slug = models.SlugField(unique=True, default='')

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.title

    
    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})
    

class Post(models.Model):
    title = models.CharField("Заголовок",max_length=255)
    summary = models.TextField("Описание")
    content = models.TextField("Контент")
    slug = models.SlugField(unique=True, default='')
    image = models.ImageField("Картинка", null=True, blank=True, upload_to='posts/')
    date = models.DateTimeField("Дата", default = timezone.now)
    categories = models.ManyToManyField(Category, related_name='posts')

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости" 

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})
    
class Comment(models.Model):
    name = models.CharField("Имя автора", max_length=255)
    email = models.EmailField("Email автора")
    text = models.TextField("Текст комментария")
    status = models.BooleanField("Статус публикации", default=0)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateTimeField("Дата комментария", default = timezone.now)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"
    
    def __str__(self):
        return self.name

class Contact(models.Model):
    name = models.CharField("ФИО", max_length=255)
    email = models.EmailField("Email")
    phone = models.TextField("Телефон", max_length=13)
    text = models.TextField("Сообщение")
    date = models.DateTimeField("Дата сообщения", default = timezone.now)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return 'Сообщение от {}'.format(self.name)
    
