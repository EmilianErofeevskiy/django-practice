from django import template
from django.template import Library
from django.shortcuts import render
from blog.models import Comment, Post, Category,Contact
from blog.forms import ContactForm
from django.utils import timezone

register = Library()

@register.inclusion_tag('blog/layout/footer_categories_list.html')
def footer_categories_list():
    catty = Category.objects.order_by('title')[:10]
    return {'catty':catty}
