from django import template
from django.template import Library
from django.shortcuts import render, redirect
from blog.models import Comment, Post, Category,Contact
from blog.forms import ContactForm
from django.utils import timezone

register = Library()

@register.inclusion_tag('blog/layout/footer_contact_form.html', takes_context=True)
def contact(context): #Form for saving into the database, but wothout sending email, related from forms.py and urls.py
    form = ContactForm()
    #Here is only view, but logic into the views.py
    return {'form':form}
