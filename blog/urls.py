from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('news/', views.posts, name="posts"),
    path('news/<slug:slug>/', views.detail, name="detail"),
    path('news/category/<slug:slug>/', views.cat_detail, name="cat_detail"),
    path('contact_cont/', views.contact_cont, name="contact_cont"),#Don't forget slash
    path('search/', views.search_result, name="search_results"),#Get response
    path('register/', views.register, name="register")
]
